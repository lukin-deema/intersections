"use strict";
var isConsoleLog = false;
/// пересечение 2 линий и формирование линий фигуры из массива координат
function intersection(lineSection1, lineSection2) {
  /// ищет точку пересечения 2 отрезков  
  /// если нет пересечения false, если есть дает точку пересечения {x, y}
  /// lineSegments : {start: {x, y}, end: {x, y}}
  var dir1 = {x: lineSection1.end.x - lineSection1.start.x, y: lineSection1.end.y - lineSection1.start.y};
  var dir2 = {x: lineSection2.end.x - lineSection2.start.x, y: lineSection2.end.y - lineSection2.start.y};

  //считаем уравнения прямых проходящих через отрезки
  var a1 = -dir1.y;
  var b1 = +dir1.x;
  var d1 = -(a1*lineSection1.start.x + b1*lineSection1.start.y);

  var a2 = -dir2.y;
  var b2 = +dir2.x;
  var d2 = -(a2*lineSection2.start.x + b2*lineSection2.start.y);

  //подставляем концы отрезков, для выяснения в каких полуплоскотях они
  var seg1_line2_start = a2*lineSection1.start.x + b2*lineSection1.start.y + d2;
  var seg1_line2_end = a2*lineSection1.end.x + b2*lineSection1.end.y + d2;

  var seg2_line1_start = a1*lineSection2.start.x + b1*lineSection2.start.y + d1;
  var seg2_line1_end = a1*lineSection2.end.x + b1*lineSection2.end.y + d1;

  //если концы одного отрезка имеют один знак, значит он в одной полуплоскости и пересечения нет.
  if (seg1_line2_start * seg1_line2_end >= 0 || seg2_line1_start * seg2_line1_end >= 0) 
    return false;

  var u = seg1_line2_start / (seg1_line2_start - seg1_line2_end);
  var result = {
    x: lineSection1.start.x + u*dir1.x,
    y: lineSection1.start.y + u*dir1.y,
  };
  return result;
}
function makeLineSection(figure, num) {
  /// переводит массив точек в отрезки предстляющие многоугольник - lineSectionArray 
  /// result массив обьектов типа { start: {x, y, pointNumber}, end: {x, y, pointNumber}, figureNumber }
  /// где x,y координаты; pointNumber номер точки в исходном массиве точек (figure)
  /// figure : [{x, y}..]
  var array = figure.slice(0);
  array.push(figure[0]);
  var result = [];
  for (var i = 1; i < array.length; i++) {
    var line = {start: array[i-1], end: array[i]};
    line.start.pointNumber = i-1;
    line.figureNumber = num;
    line.end.pointNumber = (i != array.length - 1) ? i : 0;
    line.figureNumber = num;
    result.push(line);
  }
  return result;
}

/// уравнение прямой через номера точек и координаты точек
function directEquationPoint(p1, p2){
  // pointsNumberArr[0], pointsNumberArr[1], i
  // return true если в пересечении обоих точек участвует одна линия
  var dir1 = {x: p2.x - p1.x, 
    y: p2.y - p1.y};
  // считаем уравнения прямых проходящих через точки из pointsNumberArr
  var a = -dir1.y;
  var b = +dir1.x;
  var result = {
    a: a,
    b: b,
    d: -(a*p1.x + b*p1.y),
  };
  // если точка i удовлетворяет уровнению прямой все 3 точки на одной прямой 
  return result;
}
function directEquationNumber(n1, n2){
  return directEquationPoint(
    matrixPoints[n1], matrixPoints[n2]
  );
}
/// on true  точка k находится на отрезке между p1, p2 (через координаты и номера точек)
function pointsOnLinePoint(p1, p2, k){
  var _dE = directEquationPoint(p1, p2);
  if (Math.abs(_dE.a*k.x + _dE.b*k.y + _dE.d) > 0.001) {
    return false;
  }
  var xMin = Math.min(p1.x, p2.x);
  var xMax = Math.max(p1.x, p2.x);
  var yMin = Math.min(p1.y, p2.y);
  var yMax = Math.max(p1.y, p2.y);

  return xMin <= k.x && k.x <= xMax && yMin <= k.y && k.y <= yMax;
}
function pointsOnLineNumber(n1, n2, k){
  return pointsOnLinePoint(
    matrixPoints[n1], matrixPoints[n2], matrixPoints[k]
  );
}

/// формирование матриц смежности и массива координат матриц смежности
function matrixCreate(count) {
  /// создает матрицу многоугольника для работы с графом
  var result = [];
  for (var i = 0; i < count; i++) {
    result.push([]);
    for (var j = 0; j < count; j++) {
      result[i][j] = 0;
      if (i == j + 1 || i == j - 1) {
        result[i][j] = 1;
      }
    }
  }
  result[0][count - 1] = 1;
  result[count - 1][0] = 1;

  return result;
}
function matrixesJoin(mtrx1, mtrx2, intersectionPointsArray) {
  /// матрица смежности
  /// matrix1 матриц графа фигуры 1 (А)
  /// matrix2 матриц графа фигуры 2 (B)
  /// intersectionPointsArray массив точек пересечений (X) 
  ///   {point: {x, y}, 
  ///    line1: {start: {x, y, pointNumber}, end: {x, y, pointNumber}, figureNumber },
  ///    line2: {start: {x, y, pointNumber}, end: {x, y, pointNumber}, figureNumber }
  ///   }
  ///   x,y координаты, pointNumber номер точки в фигуре
  /// result матрица смежности графа. 
  ///        размерность сумма точек обоих фигур и точек их пересечений
  var result = [];
  var matrix1 = mtrx1.slice();
  var matrix2 = mtrx2.slice();
  var fullLenght = matrix1.length + matrix2.length + intersectionPointsArray.length;
  var i,j,k;
  
  // fill empty matrix
  for (i = 0; i < fullLenght; i++) {
    result.push([]);
    for (j = 0; j < fullLenght; j++) {
      result[i].push(0);
    }
  }
  
  // проверяет intersectionPointsArray вносит поправки в матрицу
  var editions = matrix1.length + matrix2.length; 
  for (i = 0; i < intersectionPointsArray.length; i++) {
    var line1 = intersectionPointsArray[i].line1;
    var line2 = intersectionPointsArray[i].line2;

    var l1pointStart = line1.start.pointNumber;
    var l1pointEnd = line1.end.pointNumber;
    var l2pointStart = line2.start.pointNumber;
    var l2pointEnd = line2.end.pointNumber;
    
    //добавление связей точки пересечения i с точками на пересечении линий которых она находится
    result[editions + i][l1pointStart] = 1;
    result[l1pointStart][editions + i] = 1;
    result[editions + i][l1pointEnd] = 1;
    result[l1pointEnd][editions + i] = 1;
    result[editions + i][matrix1.length + l2pointStart] = 1;
    result[matrix1.length + l2pointStart][editions + i] = 1;
    result[editions + i][matrix1.length + l2pointEnd] = 1;
    result[matrix1.length + l2pointEnd][editions + i] = 1;
    
    // внесение поправок в связи матриц matrix1 и matrix2 
    if(line1.figureNumber == 1){
      matrix1[line1.start.pointNumber][line1.end.pointNumber] = 0;
      matrix1[line1.end.pointNumber][line1.start.pointNumber] = 0;
    }else if(line1.figureNumber == 2){
      matrix2[line1.start.pointNumber][line1.end.pointNumber] = 0;
      matrix2[line1.end.pointNumber][line1.start.pointNumber] = 0;
    }
    if(line2.figureNumber == 1){
      matrix1[line2.start.pointNumber][line2.end.pointNumber] = 0;
      matrix1[line2.end.pointNumber][line2.start.pointNumber] = 0;
    }else if(line2.figureNumber == 2){
      matrix2[line2.start.pointNumber][line2.end.pointNumber] = 0;
      matrix2[line2.end.pointNumber][line2.start.pointNumber] = 0;
    }
  }

  // (!1) проверка ситуаций что точки пересечения находятся на одной прямой
  // вспомогательный метод, колво пересечений в прямой начатой из вершины i
  var intersectionPointsConectedToX = function(i){
    var count = [];
    for (j = editions; j < fullLenght; j++) {
      if (result[i][j] == 1) {
        count.push(j);
      }
    }
    return count;
  };
  // растояние между точками
  var distance = function(p1, p2) {
    return Math.sqrt(
        (p2.x-p1.x)*(p2.x-p1.x)+(p2.y-p1.y)*(p2.y-p1.y)
      );
  };
  // номера точек пересечения которые относятся к одной прямой
  // [[1],[3,4]] => 1 на одной прямой относительно точки p; 3,4 на другой
  var checkSameLine = function(pointsNumberArr, p){
    var arr = pointsNumberArr.slice();
    var result = [[0,0]];
    if(arr.length >= 2) {
      for (var i = 0; i < arr.length; i++) {
        var dE = directEquationNumber(p, arr[i]);
        var fromLine = [];
        for (var j = 0; j < arr.length; ) {
          if (Math.abs(dE.a*matrixPoints[arr[j]].x + dE.b*matrixPoints[arr[j]].y + dE.d) < 0.00001){
            fromLine.push(arr[j]);
            arr.splice(arr.indexOf(arr[j]), 1);
          } else {
            j++;
          }
        }
        result.push(fromLine);
        i = -1;
      }
    } else {
      result.push(arr);
    }
    result.shift();
    return result;
  };

  var stack = [];   // хранит серии из 2+ точек пересечения для исключения 
                    // дублирования и расчета отрезка с обоих концов   
  var checkStack = function(val) {
    return val === stackVal;
  };
  var createLineWithDistance = function(x) {
    return {  start: _start, end: x,
              distance: distance(matrixPoints[_start], matrixPoints[x]) };
  };
  var sortByDistance = function(a,b) {
    return a.distance - b.distance;
  };
  var getEndOfLine = function(line) {
    return line.end;
  };
  // внесение поправок в матрицу согласно пересечениям  
  for (i = 0; i < fullLenght; i++) {
    var pointsNumberArr = intersectionPointsConectedToX(i);
    if (pointsNumberArr.length === 0){
      continue;
    }
    var arraySameLine = checkSameLine(pointsNumberArr, i);    
    for (j = 0; j < arraySameLine.length; j++) {
      if (arraySameLine[j].length >= 2) {      
        /// если 2+ точкек пересечения
        var stackVal = arraySameLine[j].join();
        if (!stack.some(checkStack)) {
          stack[i] = stackVal;
        } else {
          /// если подобные точки пересечния встречались то можно получить 
          /// начало и конец отрезка и внести поправки  в матрицу
          var _start = i;
          var _end = stack.indexOf(stackVal);
          
          /// arraySameLine[j] точки между _start и _end
          var arraySameLineWithS_E = arraySameLine[j].slice();
          arraySameLineWithS_E.push(_start, _end);
          var distances = arraySameLineWithS_E.map(createLineWithDistance).sort(sortByDistance);
          
          /// точки в порядке их расположения на отрезке _start _end 
          /// для каждой соседней точки надо добавить связь в матрицу  
          var sortedPoints = distances.map(getEndOfLine);  
          for (k = 1; k < sortedPoints.length - 2; k++) {
            if (sortedPoints[k] == 4 ||
                sortedPoints[k - 1] == 4||
                sortedPoints[k + 1] == 4) {
            }
            result[sortedPoints[k]][sortedPoints[k - 1]] = 1;
            result[sortedPoints[k - 1]][sortedPoints[k]] = 1;
            result[sortedPoints[k]][sortedPoints[k + 1]] = 1;
            result[sortedPoints[k + 1]][sortedPoints[k]] = 1;
          }

          /// точки которые не участвуют в отрезке пропускаем
          /// ближняя точка к концу 
          /// _start = 1 _end = 0 
          /// sortedPoints = [1, 11, 12, 13, 14, 0]
          /// closestPointToEnd = 14, связь result[14][0] и result[0][14] = 1 
          ///        с остальными точким = 0 разрываем
          var closestPointToEnd = sortedPoints[sortedPoints.length - 2];
          var closestPointToStart = sortedPoints[1];
          for (k = editions; k < fullLenght; k++) {
            if (sortedPoints.indexOf(k) == -1) {
              continue;
            }
            if (k !== closestPointToEnd) {
              result[_end][k] = 0;
              result[k][_end] = 0;
            }
            if (k !== closestPointToStart) {
              result[_start][k] = 0;
              result[k][_start] = 0;
            }
          }
        } 
      } else {
        /// если одна точка пересечения
        result[arraySameLine[j][0]][i] = 1;
        result[i][arraySameLine[j][0]] = 1;
      }
    }
  }  
    
  // добавляет матрицу matrix1 в результат
  for (i = 0; i < matrix1.length; i++) {
    for (j = 0; j < matrix1[i].length; j++) {
      result[i][j] = matrix1[i][j];
    }
  }
  // добавляет матрицу matrix2 в результат
  for (i = 0; i < matrix2.length; i++) {
    for (j = 0; j < matrix2[i].length; j++) {
      result[matrix1.length + i][matrix1.length + j] = matrix2[i][j];
    }
  }


  //// проверка случаев что точки находятся на какой то из линий
  if(mtrx1.length > 0){
    for (i = 0; i < editions; i++) {
      for (j = i + 1; j < editions; j++) {
        if (result[i][j] == 1) {
          for (k = 0; k < editions; k++) {
            if (i == k || j == k) { 
              continue; 
            }       
            if (pointsOnLineNumber(i,j,k)) {
              result[i][j] = 0; result[j][i] = 0;
              result[k][j] = 1; result[j][k] = 1;
              result[k][i] = 1; result[i][k] = 1;
            }
          }
        }
      }
    }
  }
  return result;
}
function matrixPointsJoin(_figure1, _num1, _figure2, _num2, intersectionPointsArray) {
  /// делатет массив соотвествия вершин матрицы смежности графа и точек   
  /// rusult: [
  ///   { x, y координаты
  ///     fN к каким многоугольникам пренадлежит точка
  ///        "1" - только первый "1.2" прямоугольники 1 и 2
  ///     line1 [number point of start, number point of start]
  ///     line2 [number point of start, number point of start]
  ///           линии в результате персечений которых появилась точка
  ///   }
  /// ]
  var result = [];
  result = result.concat(_figure1.map(function(p) { 
    return { x: p.x, y: p.y, fN: "" + _num1 }; 
  }));
  result = result.concat(_figure2.map(function(p){ 
    return { x: p.x, y: p.y, fN: "" + _num2 }; 
  }));
  result = result.concat(intersectionPointsArray.map(function(p) { 
    return { x: p.point.x, y: p.point.y, 
               fN: p.line1.figureNumber + "." + p.line2.figureNumber,
               line1: [p.line1.start.pointNumber, p.line1.end.pointNumber],
               line2: [p.line2.start.pointNumber, p.line2.end.pointNumber]
             };  
  }));
  return result;
}

/// показ сущностей в консоль
function showMatrix(_matrix) {
  /// вывод матрицы в консоль
  var str;
  console.log((_matrix>9?"":" ") + " . 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 " );
  for (var i = 0; i < _matrix.length; i++) {
    if (i > 9){
      str = '';
    } else {
      str = '0';
    }
    var line = _matrix[i].join(',');
    console.log("" + str + i + ". " + line);
  }
}
function showPathes(pathes) {
  /// вывод в консоль циклов графа
  for (var i = 0; i < pathes.length; i++) {
    var line = pathes[i].join(',');
    console.log(i + '. ' + line);
  }
}
function showIntersectionPoint(intersectionPoint, n) {
  /// выводит информацию об отрезке 
  /// line { start: {x, y, pointNumber}, end: {x, y, pointNumber}, figureNumber  }
  /// где x,y координаты; pointNumber номер точки в исходном массиве точек (figure)
  console.log("number: " + n );
  console.log( intersectionPoint.point);
  console.log(intersectionPoint.line1.figureNumber + ". " + intersectionPoint.line1.start.pointNumber +"start[" + intersectionPoint.line1.start.x + ", " + intersectionPoint.line1.start.y + "], " + intersectionPoint.line1.end.pointNumber + "end[" + intersectionPoint.line1.end.x + ", " + intersectionPoint.line1.end.y + "]");
  console.log(intersectionPoint.line2.figureNumber + ". " + intersectionPoint.line2.start.pointNumber +"start[" + intersectionPoint.line2.start.x + ", " + intersectionPoint.line2.start.y + "], " + intersectionPoint.line2.end.pointNumber + "end[" + intersectionPoint.line2.end.x + ", " + intersectionPoint.line2.end.y + "]");
}
function showFigure(fig){
  var line = '';
  for (var i = 0; i < fig.length; i++) {
    line += i +". [x=" + fig[i].x + "; y=" + fig[i].y + "], ";
  }
  console.log(line);
}

/// глубокий поис циклов графа
function dfs(first) {
  /// алгоритм глубокого поиска, проходит граф от начальной точки по 
  /// всем вершинам. при заходе в тупик если путь составляет цикл 
  /// (следущая вершина = начальной) записывает в массив циклов.
  /// также проверяет на скрытые дубликаты из одной начальный вершины
  /// ([8 4 9 2 8] == [8 9 4 2 8])
  var i = 0;
  var chain = [first];
  while (chain.length !== 0){
    for (; i < matrix.length; i++) {
      if (matrix[chain[chain.length - 1]][i] === 1 && color[i] === 0) {
        chain.push(i);
        color[i] = 1;
        i = -1;
      }
    }
    /// check duplicates before adding 
    if(matrix[chain[chain.length - 1]][chain[0]] == 1){
      var add = true; // такого цикла нет
      for (var j = 0; j < pathes.length; j++) {
        var pa = pathes[j].slice();
        var ch = chain.slice();
        // добавляем в цикл звено, чтобы убрать проход такой же цикл с обратным проходом
        ch.push(chain[0]);  
        ch.reverse();
        if (pa.join() == ch.join()) { 
          /// если перевернутый массив уже есть то не добавляем 
          /// [8 4 9 2 8].join() == [8 9 4 2 8].reverse().join()
          add = false;
        }
      }
      if (add && chain.length > 2) {
        var path = chain.slice();
        // добавляем в цикл звено, чтобы убрать проход такой же цикл с обратным проходом
        path.push(chain[0]);

        if (!checkHiddenDuplicate(path)) {
          // дописывать в пути только уникальные с учетом разных начальных вершин
          pathes.push(path);
        }
      }
    }

    i = chain.pop();
    color[i] = 0;
    i++;
  }
}
function checkHiddenDuplicate(path) {
  // return true если найден дубликат для path
  //        false если путь уникальный
  // сравнить path с каждым pathes
  return pathes.some(function(x) {
    return checkDuplicateInPathes(x, path);
  }); 
}
function checkDuplicateInPathes(resultItem, pathItem) {
  /// сравнивает 2 пути с учетом разного пункта старта 
  /// (use in checkHardDuplicate)
  /// result false если путь уникальный, 
  ///        true если путь дублирует существующий 
  ///     [8 2 9 4 (8)] == [9 2 8 4 (9)]
  var iItem = resultItem;
  var pItem = pathItem.slice(0,-1);

  var rotated = rotate(iItem[0], pItem);
  if (rotated) {
    rotated.push(rotated[0]);
    
    if (rotated.join() == iItem.join()) {
      return true;
    }
    rotated.reverse();
    if (rotated.join() == iItem.join()) {
      return true;
    }
  }
  return false;
}
function rotate(first, arr) {
  /// приводит путь к указаной точке старта 
  /// (use in checkDuplicateInResult)
  /// 8, [9 2 8 4] => [8 2 9 4]
  var result = arr.slice();
  if (first == result[0]) { 
    return result;
  }
  if (!arr.some(function(x){ return x == first})) { // если в массиве arr нет вершины first
    return false;
  }
  
  var break_ = arr.length;
  while(result[0] !== first || break_ === 0  ){
    //on true; break_ == 0 
    var el = result.shift();
    result.push(el);
    break_--;
  }
  
  return result;
}

/// пересечение фигур 
function checkFigureIntersection(_pathes, pointArrayFigure1, figureNum1, pointArrayFigure2 ,figureNum2) {
  /// _pathes массив циклов без дубликатов (после checkHiddenDuplicate())
  /// pointArrayFigure1, pointArrayFigure2 фигуры в виде массива точек [{x,y}] 
  /// figureNum1, figureNum2 номера фигур
  /// result циклы которые содержат пересечение 2 фигур
  var result = [];
  for (var i = 0; i < _pathes.length; i++) {
    if (!checkPath(_pathes[i], pointArrayFigure1, figureNum1, pointArrayFigure2, figureNum2)) {
      continue;
    }
    result.push(_pathes[i]);
  }
  return result;
}
function checkPath(path, pointArrayFigure1, figureNum1, pointArrayFigure2, figureNum2) {
  /// проход по одному из уникальных путей [8,1,0,3,9,2]
  /// path уникальный цикл  
  /// pointArrayFigure1, pointArrayFigure2 фигуры в виде массива точек [{x,y}] 
  /// figureNum1, figureNum2 номера фигур
  /// result true цикл находится на пересечении 2 фигур
  ///        false цикл не находится на пересечении 2 фигур
  for (var i = 0; i < path.length; i++) {
    var N = matrixPoints[path[i]].fN;
    var Narr = N.split('.');
    if (Narr.length == 2) { // если она на пересечении 2 фигур значит пренадлежит обоим
      continue; 
    }
    N = (N == figureNum1) ? figureNum2 : figureNum1; // обращаем если точка пренадлежит фигуре 1 далее будем искать ее пренадлежность фигуре 2
    
    /// найти Мах(х) для фигур используя массив и построить линию 
    var lineToCheck = findLineForCheckingPointInsideFigure(path[i]);
    if (!checkPointInsideFigure(lineToCheck, N, pointArrayFigure1, pointArrayFigure2)) {
      /// если точка из одной фигуры не пренадлежит второй значит фигура не пересечение 
      return false;
    }
  }
  return true;
}
function findLineForCheckingPointInsideFigure(pointNum) {
  /// строит отрезок из точки. для определения находится ли точки внутри фигуры 
  /// берет максимум Х из всех точек обоих фигур
  /// pointNum номер точка для проверки
  /// pointsArray массив точек представляющих многоугольник 
  /// result линия паралельна оси Х для нахождения количества пересечений с фигурой.
  ///        обьект типа { start: {x, y}, end: {x, y} }
  var max = matrixPoints[0].x;
  var point = matrixPoints[pointNum];
  for (var i = 0; i < matrixPoints.length; i++) {
    if (matrixPoints[i].x > max){
      max = matrixPoints[i].x;
    }
  }
  return  { 
    start: { x: point.x, y: point.y },
    end: { x: max + 0.001, y: point.y } 
  };
}
function checkPointInsideFigure(lineToCheck, figureNum, pointArrayFigure1, pointArrayFigure2) {
  /// lineToCheck линия findLineForChakingPointInsideFigure
  ///        { start: {x,y}, end:{x,y} }
  /// figureNum номер фигуры 
  /// return true если точка внутки фигуры figureNum 
  ///        false  точка снаружи фигуры
  
  /// алгоритм: найти вершины фигуры figureNum из matrixPoints
  var fig = figureNum == 1 ? pointArrayFigure1 : pointArrayFigure2;
  /// перевести фигуру из точек в отрезки
  var lineSectionArray = makeLineSection(fig, figureNum);
  /// найти кочество пересечений отрезка lineToCheck с линиями состовляющих фигуру 
  var count = 0;

  for (var i = 0; i < lineSectionArray.length; i++) {
    if(pointsOnLinePoint(lineSectionArray[i].start, lineSectionArray[i].end, lineToCheck.start)){
      return  true;
    }
    if(intersection(lineToCheck, lineSectionArray[i])){
      count++;
    }
  }
  
   // случай когда точки фигуры находятся на линии (частные случаи)
  var pointOnLine = [];
  for (i = 0; i < fig.length; i++) {
    if(isPointOnLine(fig[i], lineToCheck)){
      pointOnLine.push(i);
    }
  }

  if(specialCase(fig, pointOnLine)){
    count++;
  }

  /// если пересечений нечетное колво значит точка lineToCheck.start она же 
  ///     matrixPoints[path[i]] из метода checkPath
  return count % 2;
}
function isPointOnLine(point, line) {
  /// если вектор [line.end, line.end] коллинеарен вектору [point, line.end] 
  /// значит точка на прямой и координаты point между точками начала и конца вектора
  /// return true если точка находится на линии
  var vector1 = { x: line.end.x - line.start.x, y: line.end.y - line.start.y};
  var vector2 = { x: line.end.x - point.x, y: line.end.y - point.y};
  var mult = vector1.x * vector2.y - vector2.x * vector1.y;
  return mult === 0 && line.start.x < point.x && point.x < line.end.x;
}
function specialCase(fig, pointOnLine){
  /// обработка частных случаев лучевого алгоритма принадлежности точки многоугольнику
  /// 6 частных случае частный случай: одна точка пересечение, касание сверху, 
  ///   касание снизу, отрезок на ОХ пересечение, касание сверху, касание снизу
  var p1; // точка перед точкой на ОХ 
  var p2; // точка после точки на ОХ 
  var p;  // точка пересечения с ОХ

  var sections = devidePointOnLineArrayOnSection(pointOnLine);
  for (var i = 0; i < sections.length; i++) {
    if (sections[i].length == 1) {
      /// частный случай когда простое пересечение или касание
      p1 = fig[sections[i][0]-1] || fig[fig.length - 1];
      p2 = fig[sections[i][0]+1] || fig[0];
    } else {
      /// частный случай когда один отрезок лежит на луче ОХ
      p1 = fig[sections[i][0]-1] || fig[fig.length - 1];
      var idx = sections[i][0] + 2;
      if (fig.length - 2 == sections[i][0]) {
        idx = 0;
      }
      if (fig.length - 1 == sections[i][0]) {
        idx = 1;
      }
      p2 = fig[idx];
    }
    p = fig[pointOnLine[i]];
    return (p.y-p1.y)*(p.y-p2.y) < 0; //точки p1 и р2 по разные стороны от луча РХ
  }
}
function devidePointOnLineArrayOnSection(arr) {
  /// для разделения частных случаев лучевого алгоритма принадлежности точки многоугольнику
  /// на одну точку пересечения(случаи 1-3) либо отрезок на луче (4-6)
  
  // in  [1, 3, 4, 6, 7, 9];
  // out [[1],[3,4],[6,7],[9];
  var result = [];
  for (var i = 0; i < arr.length; i++) {
    var subArr=[];
    if (arr[i+1]!==undefined && arr[i+1] === arr[i] + 1) {
      subArr.push(arr[i]);
      subArr.push(arr[i+1]);
      i++;
    } else {
      subArr.push(arr[i]);
    }
    result.push(subArr);
  }
  return result;
}

/// на случай пересечения фигуры с самой собой => разбиваем фигуру на части 
/// и ищем пересечения каждой части с другой фигурой или ее частями
function selfIntersection(fig) {
  // return массив фигур которые получаются при разбиении фигуры
  var result = [[1,2]];
  var lineSectionArray = makeLineSection(fig, 2);  
  var matrix1 = matrixCreate(lineSectionArray.length);
  ///если одинаковые массивы считает пересечение [start, end] и [end, start]
  var intersectionPointsArray = getIntersectionPointsArray(lineSectionArray, lineSectionArray, false);

  if (intersectionPointsArray.length === 0) {
    result.push(fig);
    result.shift();
    return result;
  }

  matrixPoints = matrixPointsJoin([], undefined, fig, 2,  intersectionPointsArray);//[]  
  
  matrix = matrixesJoin([], matrix1, intersectionPointsArray);
  // console.log('self int')
  // showMatrix(matrix);
  for (var j = 0; j < matrix.length; j++) { 
    color.push(0);
  }

  for (var i = matrix1.length; 
           i < matrix1.length + intersectionPointsArray.length; 
           i++) {
    color = color.map(function() { return 0; });
    color[i] = 1;
    dfs(i);
  } 

  for (i = 0; i < pathes.length; i++) {
    var newFigure = [];
    for (j = 0; j < pathes[i].length; j++) {
      newFigure.push({ x: matrixPoints[pathes[i][j]].x, y: matrixPoints[pathes[i][j]].y });
    }
    result.push(newFigure.slice(0, -1));
  }

  result.shift();
  return result;
}
function intersectionOfSimpleFigures(fig1, num1, fig2, num2) {
  /// return многоугольник который есть пересечение исходных фигур и рисуется в drawPath 
  pathes = [];
  calculatePathes(fig1, num1, fig2, num2);
  var intersectionPathes = checkFigureIntersection(pathes, fig1, num1, fig2, num2);

  if (isConsoleLog) {
    console.log('intersectionOfSimpleFigures');
    showMatrix(matrix);
    console.log(' all ');
    showPathes(pathes);
    console.log('inters:');
    showPathes(intersectionPathes);
  }

  return intersectionPathes;
}
function calculatePathes(fig1, num1, fig2, num2) {
  /// заполниет массив pathes для фигур fig1, fig2
  /// обновляет глобальные массивы matrixPoints, matrix, color
  var lineSectionArray1 = makeLineSection(fig1, num1);
  var lineSectionArray2 = makeLineSection(fig2, num2);
  var matrix1 = matrixCreate(lineSectionArray1.length);
  var matrix2 = matrixCreate(lineSectionArray2.length);
  var intersectionPointsArray = getIntersectionPointsArray(lineSectionArray1, lineSectionArray2);

  // координаты точек
  matrixPoints = matrixPointsJoin(fig1, num1, fig2, num2, intersectionPointsArray);//[]
  // матрица смежностей
  matrix = matrixesJoin(matrix1, matrix2, intersectionPointsArray);
  
  for (var j = 0; j < matrix.length; j++) { 
    color.push(0); 
  }

  var startArr = [];
  for (var i = matrix1.length + matrix2.length; 
    i < matrix1.length + matrix2.length + intersectionPointsArray.length; 
    i++){
    startArr.push(i);
  }

  if(startArr.length === 0){
    for (i = 0; i < matrix1.length; i++) {
      for (j = matrix1.length; j < matrix1.length + matrix2.length; j++) {
        if(matrix[i][j] == 1){          
          startArr.push(i);
          j = matrix1.length + matrix2.length;
        }
      }
    }
  }

  for (i = 0; i < startArr.length; i++) {
    color = color.map(function(){ return 0; });
    color[startArr[i]] = 1;
    dfs(startArr[i]);
  }
}
function getIntersectionPointsArray(lineSectionArray1, lineSectionArray2, fullSearch) {
  /// fullSearch = false в случае если надо искать пересечение в той же фигуре, 
  /// при разных фигурах третий параметр не нужен
  if(fullSearch === undefined) { fullSearch = true; } 
  var n = 0;
  var result = [];
  for (var i = 0; i < lineSectionArray1.length; i++) {
    for (var j =  fullSearch ? 0 : i; j < lineSectionArray2.length; j++) {
      var _intersection = intersection(lineSectionArray1[i], lineSectionArray2[j]);
      if(_intersection){
        var intersectionPoint = { 
          point: _intersection, 
          line1: lineSectionArray1[i],
          line2: lineSectionArray2[j]
        };
        result.push(intersectionPoint);
        if (isConsoleLog) {
          showIntersectionPoint(intersectionPoint, n);
        }
        n++;
      }
    }
  }
  return result;
}

var matrix;       //[][]
var matrixPoints; //[]
var color = [];   // for dfs
var pathes = [];  // for dfs

function intersects(figure1, figure2) {
  var fig1 = figure1;   // [{x,y}]
  var fig2 = figure2;
  var intersectionPathes = [];
  var devidedFigure1 = selfIntersection(fig1);
  var devidedFigure2 = selfIntersection(fig2);
  
  // console.log('1');
  // for (var i = 0; i < devidedFigure1.length; i++) {
  //   showFigure(devidedFigure1[i]);    
  // }
  // console.log('2');
  // for (var i = 0; i < devidedFigure2.length; i++) {
  //   showFigure(devidedFigure2[i]);    
  // }
  var createPoint = function(p) {
    return {x: matrixPoints[p].x, y: matrixPoints[p].y };
  };
  for (var i = 0; i < devidedFigure1.length; i++) {
    for (var j = 0; j < devidedFigure2.length; j++) {
      var _pathes = intersectionOfSimpleFigures(devidedFigure1[i], 1, devidedFigure2[j], 2);
      for (var k = 0; k < _pathes.length; k++) {
        intersectionPathes.push(_pathes[k].slice(0,-1).map(createPoint));
      }
    }
  }
  // intersectionPathes = intersectionOfSimpleFigures(fig1, 1, fig2, 2).slice(0,-1).map( p => { return {x: matrixPoints[p].x, y: matrixPoints[p].y}});
  
  // перевод фигур пересечений из номеров точек в координаты
  var intersectionFigure = [];
  for (i = 0; i < intersectionPathes.length; i++) {
    intersectionFigure.push(intersectionPathes[i]);
  }
  return intersectionFigure;
}