// пример многоугольников
var examples7 = { //+
  first : [ 
    { x: 200, y: 100 }, { x: 400, y: 300 },
    { x: 300, y: 600 }, { x: 100, y: 400 },
  ],
  second : [
    { x: 200, y: 000 }, { x: 400, y: 100 },
    { x: 500, y: 400 }, { x: 100, y: 500  },
  ]
};
var examples6 = { //+
  first : [ 
    { x: 300, y: 0   }, { x: 300, y: 300 },
    { x: 230, y: 180 }, { x: 210, y: 180 },
  ],
  second : [
    { x: 220,  y: 240 }, { x: 330, y: 240 },
    { x: 330, y: 210 }, { x: 270, y: 90  },
  ]
};
var examples5 = { //+
  first : [
    { x: 100,  y: 100 }, { x: 400,  y: 200 },
    { x: 500,  y: 500 }, { x: 700,  y: 200 },
    { x: 800,  y: 400 }, { x: 700,  y: 800 },
    { x: 400,  y: 800 }, { x: 200,  y: 700 },
  ],
  second : [
    { x: 300,  y: 300 }, { x: 400,  y: 500 },
    { x: 600,  y: 200 }, { x: 700,  y: 500 },
    { x: 500,  y: 900 }, { x: 300,  y: 900 },
    { x: 100,  y: 800 }, { x: 100,  y: 500 },
  ]
};
var examples4 = { //+
  first : [
    { x: 300,  y: 100 }, { x: 500,  y: 100 },
    { x: 700,  y: 500 }, { x: 500,  y: 700 },
    { x: 200,  y: 800 }, { x: 100,  y: 300 },
  ],
  second : [
    { x: 700,  y: 200 }, { x: 900,  y: 600 },
    { x: 300,  y: 800 }, { x: 400,  y: 400 },
  ]
};
var examples3 = { //+
  first : [
    { x: 100,  y: 300 }, { x: 700,  y: 300 },
    { x: 700,  y: 500 }, { x: 100,  y: 500 },
  ],
  second : [
    { x: 300,  y: 200 }, { x: 500,  y: 200 },
    { x: 500,  y: 700 }, { x: 300,  y: 700 },
  ]
};
var examples2 = { //+
  first : [ 
    { x: 300,  y: 100 }, { x: 800,  y: 100 },
    { x: 800,  y: 300 }, { x: 300,  y: 300 },
  ],
  second : [
    { x: 500,  y: 200 }, { x: 700,  y: 200 },
    { x: 700,  y: 400 }, { x: 500,  y: 400 },
  ]
};
var examples1 = { //+
  first : [
    { x: 0,   y: 0   }, { x: 200, y: 0   },
    { x: 200, y: 500 }, { x: 0,   y: 500 },
  ],
  second : [
    { x: 100, y: 400 }, { x: 300, y: 400 },
    { x: 300, y: 600 }, { x: 100, y: 600 },
  ]
};
var examples0 = { //+
  first : [ 
    { x: 60,  y: 60  }, { x: 180, y: 0   },
    { x: 300, y: 60  }, { x: 300, y: 300 },
    { x: 240, y: 180 }, { x: 210, y: 180 },
    { x: 180, y: 240 }, { x: 150, y: 180 },
    { x: 120, y: 180 }, { x: 60,  y: 300 },
  ],
  second : [
    { x: 30,  y: 240 }, { x: 330, y: 240 },
    { x: 330, y: 210 }, { x: 270, y: 90  },
    { x: 210, y: 270 }, { x: 210, y: 90  },
    { x: 180, y: 60  }, { x: 150, y: 90  },
    { x: 150, y: 270 }, { x: 90,  y: 90  },
    { x: 30,  y: 210 }
  ]
};
var examples001 = { //+ левая сторона examples0
  first : [ 
    { x: 60,  y: 0   }, /*{ x: 180, y: 0   },
    { x: 300, y: 60  }, { x: 300, y: 300 },
    { x: 240, y: 180 }, { x: 210, y: 180 },
    { x: 180, y: 240 },*/ { x: 150, y: 180 },
    { x: 120, y: 180 }, { x: 60,  y: 300 },
  ],
  second : [
    { x: 140, y: 240 }, { x: 30, y: 240 },
    { x: 30,  y: 210 }, { x: 90, y: 90  },
  ]
};
var examples002 = { //+ правая сторона examples0
  first : [ 
    { x: 300, y: 0   }, { x: 300, y: 300 },
    { x: 240, y: 180 }, { x: 210, y: 180 },
  ],
  second : [
    { x: 220,  y: 240 }, { x: 330, y: 240 },
    { x: 330, y: 210 }, { x: 270, y: 90  },
  ]
};
var examples003 = { //+ центр сторона examples0
  first : [ 
    { x: 60, y: 60   }, { x: 180, y: 0   },
    { x: 300, y: 60  }, { x: 240, y: 180 },
    { x: 210, y: 180 }, { x: 180, y: 240 },
    { x: 150, y: 180 }, { x: 120, y: 180 },
  ],
  second : [
    { x: 150, y: 90  }, { x: 180, y: 60   },
    { x: 210, y: 90  }, { x: 210, y: 240  },
    { x: 150, y: 240 }, 
  ]
};
var examples_1 = { //one figure -
  first : [
    { x: 100,  y: 100 }, { x: 300,  y: 100 },
    { x: 300,  y: 600 }, { x: 600,  y: 600 },
    { x: 600,  y: 300 }, { x: 100,  y: 300 },
  ],
  second : [
    { x: 300,  y: 300 }
  ]
};
var examples_2 = { //one figure -
  first : [
    { x: 0,    y: 0   }, { x: 200,  y: 0 },
    { x: 200,  y: 600 }, { x: 0,    y: 600 },
    { x: 0,    y: 400 }, { x: 400,  y: 300 },
    { x: 400,  y: 200 }, { x: 0,    y: 200 },
  ],
  second : [
    { x: 300,  y: 300 }
  ]
};
var examples_3 = { //one figure-
  first : [
    { x: 100,  y: 400 }, { x: 200,  y: 100 },
    { x: 300,  y: 500 }, { x: 400, y: 100 },
    { x: 500,  y: 400 }
  ],
  second : [
    { x: 300,  y: 300 }
  ]
};

var examples = examples0;

function drawPath(data, container, color) {
  var path = document.createElementNS('http://www.w3.org/2000/svg', 'path');
  var str = 'M' + data[0].x + ',' + data[0].y+' ';
  str += data.slice(1).map(function (point) {
    return 'L' + point.x + ',' + point.y;
  }).join(' ');
  str += 'L' + data[0].x + ',' + data[0].y+' ';
  path.setAttribute('d', str);
  path.style.fill = color;
  container.appendChild(path);
}

drawPath(examples.first, document.querySelector('svg.base'), 'navy');
drawPath(examples.second, document.querySelector('svg.base'), 'yellow');

intersects(examples.first, examples.second).forEach(function (p) {
  drawPath(p, document.querySelector('svg.intersections'), 'red');
})